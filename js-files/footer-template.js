let myLocationFooter = location.host;
class Footer extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    this.innerHTML = `
    <footer>
        <nav class="footer-nav">
            <ul>
                <li>
                    <a href="https://www.linkedin.com/in/alexandru-constantin-64b767139/" target="_blank" rel="noopener noreferrer">
                        <img src="http://${myLocationFooter}/photo-gallery/resources/png/linkedin-min.png" alt="linkedin link">
                        <span>Follow me</span>
                    </a>
                </li>
                <li>
                    <a href="mailto:alexandru.konstantin@gmail.com">
                        <img src="http://${myLocationFooter}/photo-gallery/resources/png/gmail-min.png" alt="gmail link">
                        <span>Send me an E-mail</span>
                    </a>
                </li>
                <li>
                    <a
                        href="https://www.facebook.com/sharer/sharer.php?u=http://127.0.0.1:5501/photo-gallery/index.html" target="_blank" rel="noopener noreferrer">
                        <img src="http://${myLocationFooter}/photo-gallery/resources/png/share-min.png" alt="share on facebook link">
                        <span>Share with others</span>
                    </a>
                </li>
            </ul>

        </nav>
    </footer>
    `;
  }
}

customElements.define("footer-component", Footer);
