let myLocationHeader = location.host;
class Header extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    this.innerHTML = `
    <header>
        <nav class="first-nav">
            <div>
                <a href="http://${myLocationHeader}/photo-gallery/index.html">
                    <img class="logo-img" src="http://${myLocationHeader}/photo-gallery/resources/png/logo-min.png" alt="photo gallery logo" />
                </a>
            </div>
            <ul class="first-nav-list">
                <!--<li>
                    <a href=""> You </a>
                </li>-->
                <li>
                    <a href="http://${myLocationHeader}/photo-gallery/html-files/photo-portofolio.html"> Explore </a>
                </li>
                <li>
                    <a href="http://${myLocationHeader}/photo-gallery/html-files/contribution-info.html"> Contribute </a>
                </li>
            </ul>
        </nav>
        <nav class="second-nav">
            <ul class="second-nav-list">
                <li class="not-active">
                    <a href="">
                        <img class="notifications-img" src="http://${myLocationHeader}/photo-gallery/resources/png/notifications-icon-min.png"
                            alt="notifications icon" />
                    </a>
                </li>
                <li class="not-active">
                    <a href="http://${myLocationHeader}/photo-gallery/html-files/account-details.html">
                        <img class="profile-img" src="http://${myLocationHeader}/photo-gallery/resources/png/profile-icon-min.png" alt="profile icon" />
                    </a>
                </li>
            </ul>
        </nav>
    </header>
    `;
  }
}

customElements.define("header-component", Header);
